package net.oldervoll;

import net.oldervoll.domain.Player;
import net.oldervoll.domain.Team;
import net.oldervoll.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.*;

@SpringBootApplication
public class Application {

    @Autowired
    private TeamRepository teamRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @PostConstruct
    public void init() {
        List<Team> list = new ArrayList<>();

        Set<Player> players = new HashSet<>();
        players.add(new Player("Ole", "Pos1"));
        players.add(new Player("Dole", "Pos2"));
        players.add(new Player("Doffen", "Pos3"));

        list.add(new Team("Globetrotters", "Harlem", players));

        players = new HashSet<>();
        players.add(new Player("Hetti", "Pos4"));
        players.add(new Player("Letti", "Pos5"));
        players.add(new Player("Netti", "Pos6"));

        list.add(new Team("Generals", "Washington", players));

        teamRepository.save(list);
    }
}
