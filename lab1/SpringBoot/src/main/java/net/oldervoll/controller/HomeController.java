package net.oldervoll.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by frodeoldervoll on 16/05/16.
 */
@Controller
public class HomeController {
    @RequestMapping("/")
    public String hello() {
        return "hello";
    }
}
