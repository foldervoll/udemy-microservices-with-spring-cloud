package net.oldervoll.repository;

import net.oldervoll.domain.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by frodeoldervoll on 16/05/16.
 */
@RestResource(path = "teams", rel = "team")
public interface TeamRepository extends CrudRepository<Team, Long> {
}
